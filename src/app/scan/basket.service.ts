import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Articul } from '../shared/articul';
import { Item } from '../shared/item';
import { ArticulService } from '../shared/articul.service';

@Injectable({
  providedIn: 'root'
})
export class BasketService {

  basket = new BehaviorSubject<Array<Item>>([]);

  constructor() { }

  getItems(): BehaviorSubject<Array<Item>> {
    return this.basket;
  }

  addItem(articul: Articul) {
    let items = this.basket.getValue();
    console.log(items);
    let item = items.filter(i => i.articul.code == articul.code);
    if (item.length) return; //артикул уже в корзине
    items.push(new Item(articul, 1));
    this.basket.next([...items]);
  }
  updateItem(item: Item) {
    let items = this.basket.getValue();
    let i = items.filter(i => i.articul.code == item.articul.code)[0];
    i.amount = item.amount;
    this.basket.next([...items]);
  }

  removeItem(item: Item) {
    let items = this.basket.getValue();
    this.basket.next([...items.filter(i => i.articul.code != item.articul.code)]);
  }
}
