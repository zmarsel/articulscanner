import { Component, OnInit, ViewChild, ElementRef, ɵConsole } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { BasketService } from './basket.service';
import { Subject, BehaviorSubject, Subscription } from 'rxjs';
import { ToastController } from '@ionic/angular';
import { ArticulService } from '../shared/articul.service';
import { Item } from '../shared/item';

@Component({
  selector: 'app-scan',
  templateUrl: './scan.page.html',
  styleUrls: ['./scan.page.scss'],
})
export class ScanPage implements OnInit {

  constructor(private qrScanner: QRScanner, 
    private toastController: ToastController,
    private basketService: BasketService,
    private articulService: ArticulService) { }

  $code = new BehaviorSubject('');
  isScaning = false;
  text = '';
  
  $scanSub : Subscription;

  isInputFocused = false;
  articul: string;
  $basket: BehaviorSubject<Item[]>;

  ngOnInit() {
    this.$basket = this.basketService.getItems();
  }
  startScanner() {
    window.document.querySelector('html').classList.add('qr-scanner-open');
    this.qrScanner.show();
    this.isScaning = true;

  }
  closeScanner(){
    if(this.$scanSub) this.$scanSub.unsubscribe();
    window.document.querySelector('html').classList.remove('qr-scanner-open');
    this.qrScanner.hide();
    this.isScaning = false;
  }
  scan() {
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          this.text = 'camera permission was granted';
          this.startScanner();
          this.$scanSub = this.qrScanner.scan().subscribe((text: string) => {
            // window.document.querySelector('ion-app').classList.remove('cameraView');
            this.$code.next(text);

            this.toastController.create({
              message: text,
              duration: 2000
            }).then(toast => toast.present());

            this.closeScanner();
          });
        } else {
          if (status.denied) {
            this.text = "denied";
            // camera permission was permanently denied
            // you must use QRScanner.openSettings() method to guide the user to the settings page
            // then they can grant the permission from there
          } else {
            this.text = " You can ask for permission again at a later time";
            // permission was denied, but not permanently. You can ask for permission again at a later time.
          }
        }
      })
      .catch((e: any) => console.log('Error is', e));

    //this.upload.nativeElement.click();
  }

  addToCart(){
    this.articulService.search(this.articul).then(x=>{
      console.log(x);
      this.basketService.addItem(x);
    });
    this.articul = "";
  }

  decrement(item: Item) {
    item.amount--;
    if (item.amount < 0) item.amount = 0;
    this.basketService.updateItem(item);
  }

  increment(item: Item) {
    item.amount++;
    if (item.amount < 0) item.amount = 1;
    this.basketService.updateItem(item);
  }
  update(item: Item, amount: number){
    if (+amount < 0) amount = 1;
    item.amount = +amount;
    this.basketService.updateItem(item);
  }

  ionViewWillLeave(){
    window.document.querySelector('ion-app').classList.remove('cameraView');
  }
}
