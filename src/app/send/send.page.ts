import { Component, OnInit } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { BasketService } from '../scan/basket.service';
import { BehaviorSubject } from 'rxjs';
import { Item } from '../shared/item';

@Component({
  selector: 'app-send',
  templateUrl: './send.page.html',
  styleUrls: ['./send.page.scss'],
})
export class SendPage implements OnInit {
  
  $basket: BehaviorSubject<Item[]>;

  constructor(private qrScanner: QRScanner, private basketService: BasketService,
    ) { }

  ngOnInit() {
    this.$basket = this.basketService.getItems();
  }

  getJson(): string {
    return JSON.stringify(this.$basket.getValue());
  }
}
