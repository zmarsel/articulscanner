import { TestBed } from '@angular/core/testing';

import { ArticulService } from './articul.service';

describe('ArticulService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArticulService = TestBed.get(ArticulService);
    expect(service).toBeTruthy();
  });
});
