import { Injectable } from '@angular/core';
import { Articul } from './articul';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class ArticulService {

  constructor() { }

  search(code: string) : Promise<Articul>{
    // query to firebase
    return new Promise((resolve, reject)=>{
      resolve(new Articul(code, ''));
    })
  }

}
