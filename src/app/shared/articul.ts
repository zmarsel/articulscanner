export class Articul {
    constructor(code?: string, name?: string) {
        this.code = code;
        this.name = name;
    }
    code: string;
    name: string;
}

// export class ArticulPrice {
//     code: string;
//     type: string;
//     description: string;
//     price: number;
// }

