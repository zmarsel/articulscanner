import { Articul } from './articul';

export class Item {
    articul: Articul;
    amount: number;

    constructor(articul?: Articul, amount?: number){
        this.articul = articul;
        this.amount = amount;
    }
    

    getSum(): number {
        if (!!this.articul) return 10; //this.articul.price * this.amount;
        return 0;
    }
}
